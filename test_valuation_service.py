from valuation_service import readDataToDict, readProductsList, sortForSplitting, splitByMatchingId
import unittest
import os


class TestValuationService(unittest.TestCase):
    def setUp(self):
        with open('test_set_currencies.csv',mode='w') as f:
            f.write('currency,' + 'ratio,' +'\n')
            f.write('ABC,' + '4.5,' + '\n')
            f.write('DEF,' + '1.2,' + '\n')
            f.write('GHI,' + '2,' + '\n')
        with open('test_set_data.csv',mode='w') as f:
            f.write('id,' + 'price,' + 'currency,' + 'quantity,' + 'matching_id'+'\n')
            f.write('1,' + '4500,' + 'EU,' + '9,' + '2' + '\n')
            f.write('2,' + '1000,' + 'GBP,' + '1,' + '1' + '\n')
            f.write('3,' + '2500,' + 'PLN,' + '2,' + '2' + '\n')

    def test_loading(self):
        self.assertEqual(readDataToDict('test_set_currencies','currency', 'ratio', float),{'ABC': 4.5, 'DEF': 1.2, 'GHI': 2})

    def test_reading_products_list(self):
        self.assertListEqual(readProductsList('test_set_data'),
                             [['1', 85050.0, 'EU', 9, '2'], ['2', 2400, 'GBP', 1 ,'1'], ['3',5000,'PLN',2, '2']])

    def test_sorting(self):
        li = [[1,2,3,4,4,6], [5,3,2,2,4,1],[5,2,6,2,1,1]]
        sortForSplitting(li)
        self.assertListEqual(li, [[5,2,6,2,1,1], [5,3,2,2,4,1], [1,2,3,4,4,6]])

    def test_splitting(self):
        li = [[1, 2, 3, 4, 2, 6], [5, 3, 2, 2, 2, 1], [5, 2, 6, 2, 1, 1]]
        sortForSplitting(li)
        self.assertListEqual(splitByMatchingId(li), [[[5,2,6,2,1,1]], [[5,3,2,2,2,1], [1,2,3,4,2,6]]])

    def tearDown(self):
        os.remove('test_set_currencies.csv')
        os.remove('test_set_data.csv')


if __name__ == '__main__':
    unittest.main()