import csv
from operator import itemgetter

def readDataToDict(fileName,key,value, conversion):
    tempDict = {}
    with open(fileName + '.csv') as f:
        reader = csv.DictReader(f)
        for row in reader:
            tempDict[row[key]] = conversion(row[value])
    return tempDict

def readProductsList(fileName):
    productsList = []
    with open(fileName + '.csv', newline='') as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            row[1] = float(row[1]) * currencyDict[row[2]] * float(row[3])
            row[3] = int(row[3])
            productsList.append(list(row))
    return productsList

def sortForSplitting(listToSort):
    listToSort.sort(key=itemgetter(1), reverse=True)
    listToSort.sort(key=itemgetter(4))


def splitByMatchingId(listToSplit):
    splittedByMatchingId = []
    for i in range(int(listToSplit[0][4]), int(listToSplit[-1][4]) + 1):
        splittedByMatchingId.append([x for x in listToSplit if int(x[4]) == i])
    return splittedByMatchingId

def saveResults(fileName, splittedList):
    with open(fileName + '.csv', mode='w') as results:
        myWriter = csv.writer(results)
        for i in splittedList:
            productId = i[0][4]
            total, count = map(sum, (list(zip(*i[:idDict[i[0][4]]]))[1:4:2]))
            avg = total / count
            currency = 'PLN'
            skipped = len(i[idDict[i[0][4]]:])
            myWriter.writerow([productId, total, avg, currency, skipped])

currencyDict = readDataToDict('currencies', 'currency', 'ratio',float)
idDict = readDataToDict('matchings', 'matching_id', 'top_priced_count',int)
productsList = readProductsList('data')
sortForSplitting(productsList)
splittedByMatchingId = splitByMatchingId(productsList)
saveResults('results', splittedByMatchingId)