x, y = input().split()
for i in range(int(x), int(y)+1):
    print(str(i) if i % 3 != 0 and i % 5 != 0 else "Fizz" if i % 5 != 0 else "Buzz" if i % 15 != 0 else "FizzBuzz")